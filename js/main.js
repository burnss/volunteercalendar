function popDetails(shiftID, mode) {
	switch(mode) {
	case "EDIT":
		theURL = "shiftDetail.php?action=EDIT&shiftid=" + shiftID;
		break;
	case "ADDNEW":
		theURL = "shiftDetail.php?action=ADDNEW";
		break;
	default:
		theURL = "shiftDetail.php?shiftid=" + shiftID;
	}

	newWindow = window.open(theURL, "shiftDetail", "status=1, height=340, width=600, resizable=1, scrollbars=yes");
	newWindow.focus();
}

function deleteShift(shiftID) {
	var answer = confirm("Are you sure you want to delete this shift?\nDeletions CANNOT be undone.");

	if (answer)	window.location = "shiftDetail.php?action=DELETE&shiftid=" + shiftID;
}

function deleteNews(newsID, sectionID) {
	var answer = confirm("Are you sure you want to delete this news post?\nDeletions CANNOT be undone.");

	if (answer)	window.location = "index.php?action=DELETE&newsid=" + newsID + "&sectionid=" + sectionID;
}

String.prototype.fulltrim = function() {

return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,"").replace(/\s+/g," ");

}

function validateTheForm(theForm) {

	var output			= "";
	var highlightColor	= "#fee8e5";
	var count 		= 0;

	// reset form elements to white
	for (i=0; i<theForm.elements.length; i++) {
		if (theForm.elements[i].type=="text" || theForm.elements[i].type=="textarea" || theForm.elements[i].type=="select-one") {
			theForm.elements[i].style.background='#fff';
			// trim form elements removing unnecesarry white space
			theForm.elements[i].value = theForm.elements[i].value.fulltrim();
		}
	}

	// begin validating form elements
	if (theForm.fFirstName.value == "") {
		output += "- First Name\n";
		theForm.fFirstName.style.background=highlightColor;
	}
	if (theForm.fLastName.value == "") {
		output += "- Last Name\n";
		theForm.fLastName.style.background=highlightColor;
	}
	
	if (theForm.fUsername.value == "") {
		output += "- Username\n";
		theForm.fUsername.style.background=highlightColor;
	}
	
	if (theForm.fPassword.value == "") {
		output += "- Password\n";
		theForm.fPassword.style.background=highlightColor;
	}

	if (theForm.fPrimaryPhone.value == "") {
		output += "- Primary Phone\n";
		theForm.fPrimaryPhone.style.background=highlightColor;
	}

	if (theForm.fPrimaryEmail.value == "") {
		output += "- Primary Email\n";
		theForm.fPrimaryEmail.style.background=highlightColor;
	}
	else if (theForm.fPrimaryEmail.value.indexOf("@") < 0) {
		output += "- Primary Email (invalid format)\n";
		theForm.fPrimaryEmail.style.background=highlightColor;
	}
	
	if (theForm.fIsAdmin.value == 1) {
	
		for (i=0; i<theForm.elements.length; i++) {
			if (theForm.elements[i].type=="checkbox") {
				if(theForm.elements[i].checked) {
					count += 1;	
				} else {
						theForm.elements[i].style.backgroundColor=highlightColor;
				}
			}
		}
	
		if(count <= 0) {
			output += "- Please select at least one Section\n";
		}
	
	}

	// Format output (pass or fail validation)
	if (output != "") {
		output = "\nOops! The fields below are required!\n\nWe will attempt to fill in missing information automatically.\nIf you do not know the required field's information type \"n/a\" to continue.\n\n" + output;
		alert(output);
		return false;
	} 
	
	return true;	
	

}

// Same as new user validation form but without password validation
function validateUpdateForm(theForm) {

	var output			= "";
	var highlightColor	= "#fee8e5";
	var count 		= 0;

	// reset form elements to white
	for (i=0; i<theForm.elements.length; i++) {
		if (theForm.elements[i].type=="text" || theForm.elements[i].type=="textarea" || theForm.elements[i].type=="select-one") {
			theForm.elements[i].style.background='#fff';
			// trim form elements removing unnecesarry white space
			theForm.elements[i].value = theForm.elements[i].value.fulltrim();
		}
	}

	// begin validating form elements
	if (theForm.fFirstName.value == "") {
		output += "- First Name\n";
		theForm.fFirstName.style.background=highlightColor;
	}
	if (theForm.fLastName.value == "") {
		output += "- Last Name\n";
		theForm.fLastName.style.background=highlightColor;
	}
	
	if (theForm.fUsername.value == "") {
		output += "- Username\n";
		theForm.fUsername.style.background=highlightColor;
	}
	
	//if (theForm.fPassword.value == "") {
	//	output += "- Password\n";
	//	theForm.fPassword.style.background=highlightColor;
	//}

	if (theForm.fPrimaryPhone.value == "") {
		output += "- Primary Phone\n";
		theForm.fPrimaryPhone.style.background=highlightColor;
	}

	if (theForm.fPrimaryEmail.value == "") {
		output += "- Primary Email\n";
		theForm.fPrimaryEmail.style.background=highlightColor;
	}
	else if (theForm.fPrimaryEmail.value.indexOf("@") < 0) {
		output += "- Primary Email (invalid format)\n";
		theForm.fPrimaryEmail.style.background=highlightColor;
	}
	
	if (theForm.fIsAdmin.value == 1) {
	
		for (i=0; i<theForm.elements.length; i++) {
			if (theForm.elements[i].type=="checkbox") {
				if(theForm.elements[i].checked) {
					count += 1;	
				} else {
						theForm.elements[i].style.backgroundColor=highlightColor;
				}
			}
		}
	
		if(count <= 0) {
			output += "- Please select at least one Section\n";
		}
	
	}

	// Format output (pass or fail validation)
	if (output != "") {
		output = "\nOops! The fields below are required!\n\nWe will attempt to fill in missing information automatically.\nIf you do not know the required field's information type \"n/a\" to continue.\n\n" + output;
		alert(output);
		return false;
	} 
	
	return true;	
	

}

function RandomLoginGenerator(x) {
	
	var a;
	var b;
	var output;
	
	a = document.AdminUserDetails.fLastName.value;
	if (a.length > 6) a = a.substring(0,6);
	
	b = document.AdminUserDetails.fFirstName.value;
	b = b.substring(0,1);

	output = a + b
	x.value = output.toLowerCase();
	
	return true;
	
}


function RandomPasswordGenerator(x) {
	
	var theNumber;
	theNumber = Math.floor(Math.random() * 1000000);
	
	x.value = theNumber;
	
	return true;
	
}

/*
function highlightNewUser() {
	newUser = document.getElementById("1159");
	newUser.style.backgroundColor = "#fff8d0";
}
*/