<?php

  /*---------------------------------------------------------------
   * 
   * 	MODULE:		ahiftDetail.php
   * 	AUTHOR:		Gian
   * 	Created:	Unknown
   * 
   * --------------------------------------------------------------
   * 
   * 	
   * 	MODIFICATION HISTORY
   * 	20170521 PRSC	Cleanup paths, code and PHP tabs fixed.
   *    * 
   *---------------------------------------------------------------
   */


    $basepath = $_SERVER['DOCUMENT_ROOT']."/VolunteerCalendar";

	require ($basepath . "/init.php");
	require ($basepath . "/includes/adminFunctions.php");
	require ($bathpath . "/includes/functions.php");


	$shiftID = $_GET['shiftid'];
	$action = $_GET['action'];

	if (empty($action)) $action = "VIEW";

	//print "ACTION ["  . $action . "]<br>";
	
	
	switch($action) {
	case "ADDNEW":
		$pageTitle = "Add New Shift";
		$shiftDate = date("m/d/Y");
		break;
		
	case "EDIT";
		$pageTitle = "Edit Shift";
		$shiftValues = loadShiftData($shiftID);

		//print_r($shiftValues);

		$shiftUserID = $shiftValues[0];
		$shiftDate = date("m/d/Y", strtotime($shiftValues[1]));
		$shiftStart = date("g:i a", strtotime($shiftValues[1]));
		$shiftEnd = date("g:i a", strtotime($shiftValues[2]));
		$shiftComment = str_replace("'", "", strip_tags($shiftValues[3]));
		$shiftCat = $shiftValues[4];

		break;
	case "VIEW":
		$pageTitle = "View Shift";
	}

	/*...............Theoretically update the APPSHIFT table with the details........................PRSC */
	
	if (array_key_exists('fShiftDate', $_POST) || $action == "DELETE") {
		//form has been posted
		updateShift($action, getShiftFormData(), $shiftID);
		echo "<script>window.opener.location.reload(true);window.close();</script>";
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Shift Detail</title>

	<link rel="stylesheet" href="css/default.css" media="screen,projection" type="text/css" />
	<link rel="stylesheet" href="css/dhtmlgoodies_calendar.css" media="screen,projection" type="text/css" />

	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/dhtmlgoodies_calendar.js"></script>
</head>

<body id="entry">

<h1><?= $pageTitle ?></h1>


	<?php 
	
	if ($action == "ADDNEW") { ?>
		<form action="shiftDetail.php?shiftid=<?= $shiftID ?>&action=<?= $action ?>" name="shiftForm" method="post">
		<table id="detailTable" cellpadding="0" cellspacing="0" width="98%">
		<tr>
			<td class="label">Volunteer</td>
			<td><?php drawUserSelect($currentUserID, "fVolunteer", $currentUserID) ?></td>
		</tr>
		<tr>
			<td class="label">Date</td>
			<td>
				<input class="datesel" type="text" name="fShiftDate" readonly value="<?= $shiftDate ?>">
				<img src="images/icon_cal.gif" border="0" title="Calendar" onclick="displayCalendar(document.forms[0].fShiftDate, 'mm/dd/yyyy', this);" onmouseover="this.style.cursor='pointer';" />
                <img src="images/refresh.png" border="0" title="Clear Date" onclick="document.forms[0].fShiftDate.value = '';" onmouseover="this.style.cursor='pointer';" />
			</td>
		</tr>
		<tr>
			<td class="label">Start Time</td>
			<td><?php drawTimeSelect("fStartTime", $shiftStart); echo "Note: <strong>12:00 AM = Midnight | 12:00 PM = Noon</strong>"; ?></td>
		</tr>
		<tr>
			<td class="label">End Time</td>
			<td><?php drawTimeSelect("fEndTime", $shiftEnd); ?></td>
		</tr>
		<tr>
			<td class="label">Section</td>
			<td><?php printCategories("SELECT", $shiftCat, $currentUserID, false); ?></td>
		</tr>
		<tr>
			<td class="label" valign="top">Comments</td>
			<td valign="top"><textarea name="fComments"><?= $shiftComment ?></textarea></td>
		</tr>
		</table>

		<div id="buttonbar">
			<input type="submit" value="Save" />
			<input type="button" onclick="window.close();" value="Cancel" />
		</div>

		<input type="hidden" name="fShiftID" value="<?= $shiftID ?>" />

	<?php } elseif ($action == "EDIT") { ?>
		<form action="shiftDetail.php?shiftid=<?= $shiftID ?>&action=<?= $action ?>" name="shiftForm" method="post">
		<table id="detailTable" cellpadding="0" cellspacing="0" width="98%">
		<tr>
			<td class="label">Volunteer</td>
			<td><?php drawUserSelect($currentUserID, "fVolunteer", $shiftUserID) ?></td>
		</tr>
		<tr>
			<td class="label">Date</td>
			<td>
				<input class="datesel" type="text" name="fShiftDate" readonly value="<?= $shiftDate ?>">
				<img src="images/icon_cal.gif" border="0" title="Calendar" onclick="displayCalendar(document.forms[0].fShiftDate, 'mm/dd/yyyy', this);" onmouseover="this.style.cursor='pointer';" />
                <img src="images/refresh.png" border="0" title="Clear Date" onclick="document.forms[0].fShiftDate.value = '';" onmouseover="this.style.cursor='pointer';" />
			</td>
		</tr>
		<tr>
			<td class="label">Start Time</td>
			<td><?php drawTimeSelect("fStartTime", $shiftStart); echo "Note: <strong>12:00 AM = Midnight | 12:00 PM = Noon</strong>"; ?></td>
		</tr>
		<tr>
			<td class="label">End Time</td>
			<td><?php drawTimeSelect("fEndTime", $shiftEnd); ?></td>
		</tr>
		<tr>
			<td class="label">Section</td>
			<td><?php printCategories("SELECT", $shiftCat, $currentUserID, false); ?></td>
		</tr>
		<tr>
			<td class="label" valign="top">Comments</td>
			<td valign="top"><textarea name="fComments"><?= $shiftComment ?></textarea></td>
		</tr>
		</table>

		<div id="buttonbar">
			<input type="submit" value="Save" />
			<input type="button" onclick="window.close();" value="Cancel" />
		</div>

		<input type="hidden" name="fShiftID" value="<?= $shiftID ?>" />
	<?php } else {
		printShiftDetail($currentUserID, $shiftID);
	} ?>
</form>

</body>
</html>